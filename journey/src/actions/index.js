import axios from 'axios'
import { mapValues } from 'lodash'

import { resDataExtractor } from '~/utils/request.util'
import { throwIfNull } from '~/utils/flow.util'

const API_URL = 'https://band-manager-5303b.firebaseio.com'

export const FETCH_ORDERS = 'fetch-orders'
export function fetchOrders() {
    return {
        type: FETCH_ORDERS,
        payload: axios.get(`${API_URL}/orders.json`)
            .then(resDataExtractor)
            .then(orders => {
                return mapValues(orders, (order, key) => {
                    return {id: key, ...order}
                })
            })
    }
}

export const SUBMIT_ORDER = 'submit-order'
export function submitOrder(order) {
    return {
        type: SUBMIT_ORDER,
        payload: axios.post(`${API_URL}/orders.json`, order)
            .then(resDataExtractor)
            .then(({ name }) => {
                return {
                    id: name,
                    ...order
                }
            })
    }
}

export const FETCH_ORDER = 'fetch-order'
export function fetchOrder(id) {
    return {
        type: FETCH_ORDER,
        payload: axios.get(`${API_URL}/orders/${id}.json`)
            .then(resDataExtractor)
            .then(throwIfNull)
            .then(order => {
                return {
                    id,
                    ...order
                }
            })
    }
}

export const DELETE_ORDER = 'delete-order'
export function deleteOrder(id) {
    return {
        type: DELETE_ORDER,
        payload: axios.delete(`${API_URL}/orders/${id}.json`)
            .then(() => id)
    }
}

export const UPDATE_ORDER = 'update-order'
export function updateOrder(id, patch) {
    return {
        type: UPDATE_ORDER,
        payload: axios.patch(`${API_URL}/orders/${id}.json`, patch)
            .then(resDataExtractor)
    }
}