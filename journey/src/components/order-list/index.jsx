import React from 'react'

export default function OrderList(props) {
    return (
        <div>
            <h2>Order List</h2>
            <ul className="list-group">
                {props.children}
            </ul>
        </div>
    )
}