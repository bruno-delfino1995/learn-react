import React, { Component } from 'react'

import Show from './show'
import Edit from './edit'

export default class OrderListItem extends Component {
    constructor(props) {
        super(props)

        this.state = {
            underEdit: false
        }

        this.deleteOrder = this.deleteOrder.bind(this)
        this.editOrder = this.editOrder.bind(this)
        this.finishEdit = this.finishEdit.bind(this)
    }

    deleteOrder() {
        this.props.delete(this.props.data.id)
    }

    editOrder() {
        if (!this.props.canEdit) return

        this.props.beforeUpdate()
        this.setState({ underEdit: true })
    }

    finishEdit() {
        this.setState({ underEdit: false })
        this.props.afterUpdate()
    }

    render() {
        const order = this.props.data

        if (this.state.underEdit) {
            return (
                <Edit
                order={order}
                initialValues={order}
                onCancel={this.finishEdit}
                onSubmit={this.finishEdit}
                />
            )
        }

        return (
            <Show
            order={order}
            onDelete={this.deleteOrder}
            onEdit={this.editOrder}
            />
        )
    }
}