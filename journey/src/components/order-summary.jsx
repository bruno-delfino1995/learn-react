import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import { fetchOrder } from '~/actions'

class OrderSummary extends Component {
    componentWillMount() {
        this.props.fetchOrder(this.props.id)
            .catch(err => {
                console.log(err)
            })
    }

    componentWillUpdate() {
        console.log('Updating', this.props)
    }

    render() {
        const order = this.props.order

        if (!order) return <div>Loading...</div>

        return (
            <div>
                <p>ID: {order.id}</p>
                <p>Date: {order.date}</p>
                <p>Channel: {order.channel}</p>
                <p>Status: {order.status ? 'true' : 'false'}</p>
                <p>Description: {order.description}</p>
                <Link to={`/orders/${order.id}/details`}>More Details</Link>
            </div>
        )
    }
}

function mapStateToProps({ orders }, { id }) {
    return { id, order: orders[id] }
}

export default connect(mapStateToProps, { fetchOrder })(OrderSummary)