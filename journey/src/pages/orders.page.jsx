import React from 'react'
import { BrowserRouter, Route } from 'react-router-dom'

import OrderList from '~/components/order-list'
import OrderSubmission from '~/components/order-submission'
import OrderSummary from '~/components/order-summary'

import withRouteParams from '~/utils/components/with-route-params'

const OrderSummaryWithParams = withRouteParams(undefined, OrderSummary)

export default function OrdersPage(props) {
    const SubmissionAndList = (props) => (
        <div className="row">
            <div className="col-6">
                <OrderSubmission />
            </div>
            <div className="col-6">
                <OrderList />
            </div>
        </div>
    )

    return (
        <div className="row">
            <div className="col-8">
                <Route path="/orders" component={SubmissionAndList} />
            </div>
            <div className="col-4">
                <Route path="/orders/:id" component={OrderSummaryWithParams} />
            </div>
        </div>
    )
}