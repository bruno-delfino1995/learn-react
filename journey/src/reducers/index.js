import { combineReducers } from 'redux';
import ordersReducer from './orders.reducer'
import { reducer as formReducer } from 'redux-form'

const rootReducer = combineReducers({
  orders: ordersReducer,
  form: formReducer
});

export default rootReducer;
