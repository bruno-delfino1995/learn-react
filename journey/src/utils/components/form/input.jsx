import React from 'react'

export default function Input(field) {
    const hasError = field.meta.touched && field.meta.invalid
    const className = `form-group ${hasError ? 'has-danger' : ''}`

    return (
        <div className={className}>
            <label htmlFor={field.id}>{field.label}</label>
            <input id={field.id} className="form-control"
            type={field.type}
            {...field.input}
            />
            <div className="text-help">
                {field.meta.touched ? field.meta.error : ''}
            </div>
        </div>
    )
}