import React from 'react'

export default function TextArea(field) {
    const hasError = field.meta.touched && field.meta.invalid
    const className = `form-group ${hasError ? 'has-danger' : ''}`

    return (
        <div className={className}>
            <label htmlFor={field.id}>{field.label}</label>
            <textarea id={field.id} className="form-control"
            rows={field.rows ? field.rows : 3}
            {...field.input}
            />
            <div className="text-help">
                {field.meta.touched ? field.meta.error : ''}
            </div>
        </div>
    )
}