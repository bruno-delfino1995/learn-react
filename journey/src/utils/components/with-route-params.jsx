import React, { Component } from 'react'
import { assign, partial } from 'lodash'

const defaultMapper = partial(assign, {})

export default function WithRouteParams(mapper = defaultMapper, Component) {
    return function WithRouteParamsDecorator(props) {
        const {match: { params }, ...passThrough} = props

        return <Component {...mapper(params, passThrough)} />
    }
}