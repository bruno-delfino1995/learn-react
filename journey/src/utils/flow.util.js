export function throwIfNull(obj, message = "Null received") {
    if (obj == null) throw new Error(message)

    return obj
}