import axios from 'axios'

const API_URL='http://reduxblog.herokuapp.com/api'
const API_KEY='1FMIys2H'

export const FETCH_POSTS='FETCH_POSTS'
export function fetchPosts() {
    return {
        type: FETCH_POSTS,
        payload: axios.get(`${API_URL}/posts`, { params: { key: API_KEY } })
    }
}

export const CREATE_POST='CREATE_POST'
export function createPost(data, cbk) {
    return {
        type: CREATE_POST,
        payload: axios.post(`${API_URL}/posts`, data, { params: { key: API_KEY } })
            .then((val) => {
                cbk()
                return val
            })

    }
}

export const FETCH_POST = 'FETCH_POST'
export function fetchPost(id) {
    return {
        type: FETCH_POST,
        payload: axios.get(`${API_URL}/posts/${id}`, { params: { key: API_KEY } })
    }
}

export const DELETE_POST = 'DELETE_POST'
export function deletePost(id, cbk) {
    return {
        type: DELETE_POST,
        payload: axios.delete(`${API_URL}/posts/${id}`, { params: { key: API_KEY } })
            .then(() => {
                cbk()
                return id
            })
    }
}