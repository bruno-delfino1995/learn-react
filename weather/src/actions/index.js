import axios from 'axios'

const API_KEY = 'af5b404ec298103321f9b4f2bec9e8ce'
const FORECAST_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`

export const FETCH_WEATHER = 'FETCH_WEATHER'
export function fetchWeather(city) {

    return {
        type: FETCH_WEATHER,
        payload: axios.get(FORECAST_URL, { params: { q: `${city},br` } })
    }
}